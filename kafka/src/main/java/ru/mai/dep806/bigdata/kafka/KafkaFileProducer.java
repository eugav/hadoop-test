package ru.mai.dep806.bigdata.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class KafkaFileProducer {
    private static final String topic = "lines";

    public static void run() throws InterruptedException, IOException {

        String textFile = "data/texts/adams.txt";

        Properties properties = new Properties();
        properties.put("bootstrap.servers", "localhost:9092");
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String, String> producer = new KafkaProducer<>(properties);

        Files.readAllLines(Paths.get(textFile)).forEach(line -> {
            ProducerRecord<String, String> message = null;
            message = new ProducerRecord<>(topic, line);
            producer.send(message);
            System.out.println("Send '" + message + "'");
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

        producer.close();
    }

    public static void main(String[] args) {
        try {
            KafkaFileProducer.run();
        } catch (InterruptedException | IOException e) {
            System.out.println(e);
        }
    }
}
