import sys
from typing import Tuple

from pyspark.rdd import RDD
from pyspark.sql import SparkSession

if __name__ == "__main__":

    spark = SparkSession\
            .builder\
            .appName("PythonSort")\
            .getOrCreate()

    spark.sparkContext.textFile("C:/tmp/text/book.html") \
            .flatMap(lambda line: line.split()) \
            .map(lambda word: (word,1)) \
            .reduceByKey(lambda k1, k2: k1 + k2) \
            .map(lambda v: (v[1], v[0])) \
            .sortByKey(False) \
            .saveAsTextFile("c:/tmp/text_out2")

    spark.stop()