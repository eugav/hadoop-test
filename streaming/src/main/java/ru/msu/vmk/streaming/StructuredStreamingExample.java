package ru.msu.vmk.streaming;

import org.apache.spark.sql.*;
import org.apache.spark.sql.internal.SQLConf;
import org.apache.spark.sql.streaming.StreamingQuery;
import org.apache.spark.sql.streaming.StreamingQueryException;
import scala.Tuple2;

import java.util.Arrays;

public class StructuredStreamingExample {
    public static void main(String[] args) {
        SparkSession spark = SparkSession
                .builder()
                .appName("StructuredStreamingExample")
                .master("local")
                .getOrCreate();

        spark.sparkContext().setLogLevel("ERROR");

        //added to reduce number of task in stage
        spark.sessionState().conf().setConf(SQLConf.SHUFFLE_PARTITIONS(), 2);

        Dataset<String> lines = spark
                .readStream()
                .format("kafka")
                .option("kafka.bootstrap.servers", "localhost:9092")
                .option("subscribe", "wordlines")
                .load()
                .selectExpr("CAST(value AS STRING)")
                .as(Encoders.STRING());

        lines.createOrReplaceTempView("lines");

        Dataset<Tuple2> wordCount = lines
                .flatMap((String x) -> Arrays.asList(x.split(" ")).iterator(), Encoders.STRING())
                .groupBy("value")
                .count().as(Encoders.bean(Tuple2.class)).alias("count");
//                .orderBy(functions.desc("count"));

        StreamingQuery query = wordCount
                .writeStream()
                .outputMode("update")
                .format("console")
                .start();

        try {
            query.awaitTermination();
        } catch (StreamingQueryException e) {
            e.printStackTrace();
            query.stop();
        }
    }
}
