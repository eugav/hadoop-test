package ru.msu.vmk.streaming;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.spark.SparkConf;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import org.apache.kafka.common.serialization.StringDeserializer;
import scala.Tuple2;

import java.util.*;

public class SimpleStreamExample {
    public static String appName = "SimpleStreamExample";
    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName(appName);

        JavaStreamingContext streamingContext = new JavaStreamingContext(conf, Durations.seconds(10));

        streamingContext.sparkContext().setLogLevel("ERROR");

        List<String> topics = Collections.singletonList("lines");

        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "localhost:9092");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "simple-consumer");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit",false);

        JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils.createDirectStream(
                streamingContext,
                org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent(),
                org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe(topics, kafkaParams));

        JavaPairDStream<String, Integer> pairs = stream
                .flatMap(x -> Arrays.asList(x.value().split(" ")).iterator())
                .mapToPair(x -> new Tuple2<>(x, 1))
                .window(Durations.minutes(1))
                .reduceByKey((a, b) -> a + b);

        pairs.print();

        streamingContext.start();
        try {
            streamingContext.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
            streamingContext.stop();
        }
    }
}
