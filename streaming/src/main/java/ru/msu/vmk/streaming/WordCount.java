package ru.msu.vmk.streaming;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.Optional;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.streaming.Duration;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaInputDStream;
import org.apache.spark.streaming.api.java.JavaPairDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.kafka010.KafkaUtils;
import scala.Tuple2;

import java.util.*;


public class WordCount {

    public static String appName = "WordCount";

    public static void main(String[] args) {
        SparkConf conf = new SparkConf()
                .setMaster("local[*]")
                .setAppName(appName);

        JavaStreamingContext streamingContext = new JavaStreamingContext(conf, Durations.seconds(10));

        streamingContext.sparkContext().setLogLevel("ERROR");

        //A check point directory is needed by Spark for stateful stream process
        streamingContext.checkpoint("/tmp/spark_checkpoint2");

        List<String> topics = Collections.singletonList("wordlines");

        Map<String, Object> kafkaParams = new HashMap<>();
        kafkaParams.put("bootstrap.servers", "localhost:9092");
        kafkaParams.put("key.deserializer", StringDeserializer.class);
        kafkaParams.put("value.deserializer", StringDeserializer.class);
        kafkaParams.put("group.id", "simple-consumer");
        kafkaParams.put("auto.offset.reset", "latest");
        kafkaParams.put("enable.auto.commit", false);

        Function2<List<Integer>, Optional<Integer>, Optional<Integer>> updateFunction =
                (values, state) -> {
                    Integer newState = state.orElse(0) + values.stream().reduce(Integer::sum).orElse(0);
                    return Optional.of(newState);
                };

        JavaInputDStream<ConsumerRecord<String, String>> stream = KafkaUtils.createDirectStream(
                streamingContext,
                org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent(),
                org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe(topics, kafkaParams));

        stream
                .flatMap(x -> Arrays.asList(x.value().split(" ")).iterator())
                .mapToPair(x -> new Tuple2<String, Integer>(x, 1))
                .reduceByKey(Integer::sum)
                .updateStateByKey(updateFunction)
                .window(Duration.apply(30_000))
                        .saveAsNewAPIHadoopFiles("c:/tmp/words/", "words", Text.class, IntWritable.class, TextOutputFormat.class);
//                .foreachRDD(rdd -> {
//                    System.out.println(rdd.mapToPair(Tuple2::swap)
//                            .sortByKey(false)
//                                .take(10));
//                        }
//                );

        streamingContext.start();
        try {
            streamingContext.awaitTermination();
        } catch (InterruptedException e) {
            e.printStackTrace();
            streamingContext.stop();
        }
    }
}
