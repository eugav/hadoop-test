package ru.mai.dep806.bigdata.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.sql.*;
import scala.Tuple2;

public class CsvExample2 {

    public static void main(String[] args) {

        // Создание спарк-конфигурации и сессии
        SparkConf sparkConf = new SparkConf().setAppName("Spark Word Count");
        sparkConf.setMaster("local[4]");

        SparkSession session = SparkSession.builder()
                .appName("Spark CSV Processor")
                .config(sparkConf)
                .getOrCreate();
        SQLContext sqlContext = new SQLContext(session);

        // Чтение файлов из директории, указанной первым аргументом командной строки
        Dataset<Row> books = sqlContext.read().option("header", "true")
                .option("inferSchema", "true")
                .option("quote", "\"")
                .option("escape", "\"")
                .csv(args[0]);

        books.createOrReplaceTempView("books");

        // Spark SQL
        session.sql("select genre, count(*) count from books group by genre")
                .show();

        // Spark SQL DSL (Domain Specific Language)
        Column genre = books.col("genre");
        books
                .groupBy(genre)
                .agg(functions.count(books.col("title")).alias("count"))
                .select(genre, functions.col("count"))
                .show();

        books.javaRDD().mapToPair(row -> new Tuple2<>(row.getString(2), 1))
                .aggregateByKey(0, (counter, value) -> counter + value, (counter1, counter2) -> counter1 + counter2)
                .foreach(tuple -> System.out.println(tuple));

    }
}
