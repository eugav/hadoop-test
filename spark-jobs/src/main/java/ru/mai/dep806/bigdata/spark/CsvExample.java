package ru.mai.dep806.bigdata.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

/**
 * Пример Word Count на Spark / Java 8.
 *
 * Запуск на Hadoop:

 spark-submit --class ru.mai.dep806.bigdata.spark.WordCount \
 --master local \
 --deploy-mode cluster \
 --driver-memory 100M \
 --executor-memory 500M \
 spark-jobs-1.0-SNAPSHOT.jar \
 /user/stud/eugene/library \
 /user/stud/eugene/spark-wc


 DAG:

 (3) ShuffledRDD[4] at reduceByKey at WordCount.java:57 [Disk Memory Deserialized 1x Replicated]
 +-(3) MapPartitionsRDD[3] at mapToPair at WordCount.java:55 [Disk Memory Deserialized 1x Replicated]
 |  MapPartitionsRDD[2] at flatMap at WordCount.java:49 [Disk Memory Deserialized 1x Replicated]
 |  /user/stud/texts MapPartitionsRDD[1] at textFile at WordCount.java:47 [Disk Memory Deserialized 1x Replicated]
 |  /user/stud/texts HadoopRDD[0] at textFile at WordCount.java:47 [Disk Memory Deserialized 1x Replicated


spark-submit --class ru.mai.dep806.bigdata.spark.WordCount hdfs:///user/stud/eugene/spark-jobs-1.0-SNAPSHOT.jar  /user/stud/texts /user/stud/words3

 */
public class CsvExample {

    public static void main(String[] args) {

        // Создание спарк-конфигурации и сессии
        SparkConf sparkConf = new SparkConf().setAppName("Spark Word Count");
        sparkConf.setMaster("local[4]");

        SparkSession session = SparkSession.builder()
                .appName("Spark CSV Processor")
                .config(sparkConf)
                .getOrCreate();
        SQLContext sqlContext = new SQLContext(session);

        // Чтение файлов из директории, указанной первым аргументом командной строки
        Dataset<Row> df = sqlContext.read().option("header", "true")
                .option("inferSchema", "true")
                .option("quote", "\"")
                .option("escape", "\"")
                .csv(args[0]);

        JavaRDD<Row> rows = df.javaRDD();

        System.out.println(rows.take(10));

        rows.mapToPair(row -> new Tuple2<String, Integer>(row.getString(2), 1))
                .aggregateByKey(0, (counter, value) -> counter + value, (counter1, counter2) -> counter1 + counter2)
                .foreach(tuple -> System.out.println(tuple));

    }
}
