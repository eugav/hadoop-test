package ru.mai.dep806.bigdata.spark;

import org.apache.spark.sql.*;

/**
 * spark-submit --class ru.mai.dep806.bigdata.spark.TopHadoopUsers
 * --master yarn
 * --deploy-mode cluster
 * --driver-memory 100M
 * --executor-memory 500M
 * spark-jobs-1.0-SNAPSHOT.jar
 * /user/stud/stackoverflow/landing/posts_sample
 * /user/stud/stackoverflow/landing/users
 * /user/stud/eugene/top_hadoop_users
 */
public class TopUsersDataFrame {
    public static void main(String[] args) {

        String postsPath = args[0];
        String usersPath = args[1];

        SparkSession spark = SparkSession
                .builder()
                .appName("Spark Tags Popularity")
                .master("local")
                .getOrCreate();

        Dataset<Row> usersDF = spark
                .read()
                .format("com.databricks.spark.xml")
                .option("rootTag", "users")
                .option("rowTag", "row")
                .load(usersPath);

        Dataset<Row> postsDF = spark
                .read()
                .format("com.databricks.spark.xml")
                .option("rootTag", "posts")
                .option("rowTag", "row")
                .load(postsPath);

        usersDF.createOrReplaceTempView("users");
        postsDF.createOrReplaceTempView("posts");

        Dataset<Row> topHadoopUsers = spark.sql("select u._Id, u._DisplayName, count(p._Id) from posts p " +
                "inner join users u on (p._OwnerUserId = u._Id) " +
                "where p._Tags like '%transportation%' " +
                "group by u._Id, u._DisplayName " +
                "order by count(p._Id) desc");

//        Dataset<Row> topHadoopUsers = postsDF.filter("_Tags like '%python%'")
//                .groupBy("_OwnerUserId")
//                .agg(functions.count("_Id").alias("count"))
//                .join(usersDF, new Column("_OwnerUserId").equalTo(usersDF.col("_Id")))
//                .orderBy(functions.desc("count"))
//                .select("_OwnerUserId", "_DisplayName", "count");

        topHadoopUsers.show(10);
    }
}
