package ru.mai.dep806.bigdata.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.storage.StorageLevel;
import org.spark_project.guava.collect.Lists;
import scala.Tuple2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Пример Word Count на Spark / Java 8.
 *
 * Запуск на Hadoop:

 spark-submit --class ru.mai.dep806.bigdata.spark.WordCount \
 --master yarn \
 --deploy-mode cluster \
 --driver-memory 600M \
 --executor-memory 600M \
 hdfs:///user/stud/eugene/spark-jobs-1.0-SNAPSHOT.jar \
 /user/stud/textdata \
 /user/stud/eugene/spark-wc


 DAG:

 (3) ShuffledRDD[4] at reduceByKey at WordCount.java:57 [Disk Memory Deserialized 1x Replicated]
 +-(3) MapPartitionsRDD[3] at mapToPair at WordCount.java:55 [Disk Memory Deserialized 1x Replicated]
 |  MapPartitionsRDD[2] at flatMap at WordCount.java:49 [Disk Memory Deserialized 1x Replicated]
 |  /user/stud/texts MapPartitionsRDD[1] at textFile at WordCount.java:47 [Disk Memory Deserialized 1x Replicated]
 |  /user/stud/texts HadoopRDD[0] at textFile at WordCount.java:47 [Disk Memory Deserialized 1x Replicated


spark-submit --class ru.mai.dep806.bigdata.spark.WordCount --master yarn --deploy-mode cluster  --driver-memory 100M  --executor-memory 500M  hdfs:///user/stud/eugene/spark-jobs-1.0-SNAPSHOT.jar  /user/stud/textdata /user/stud/eugene/words1

 */
public class WordCount {

    public static void main(String[] args) {

//        Set<String> excludedWords = new HashSet(Arrays.asList("one", "a", "who"));

        // Создание спарк-конфигурации и контекста
        SparkConf sparkConf = new SparkConf().setAppName("Spark Word Count");
        sparkConf.setMaster("local[4]");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

//        Broadcast<Set<String>> broadcast = sc.broadcast(excludedWords);

        // Чтение файлов из директории, указанной первым аргументом командной строки
        JavaRDD<String> words = sc.textFile(args[0])
                // Для каждой строки: разделить ее на слова по пробелам и слить в общий список
                .flatMap((String line) -> Arrays.asList(line.split(" ")).iterator())
                .filter(word -> word.trim().length() > 0);
                // .filter(word -> !broadcast.getValue().contains(word.trim()));

        // В итоге words содержит список всех слов
        JavaPairRDD<String, Integer> counts =
                // Каждое слово преобразуем в пару (слово, 1). 1 - обозначает, что слово встретилось 1 раз.
                words.mapToPair(word -> new Tuple2<>(word, 1))
                        // Выполняем группировку по ключу (слову), суммируя значения
                        .reduceByKey((x, y) -> x + y)
                        .persist(StorageLevel.MEMORY_ONLY());

        // В итоге counts содержит пары (слово, кол-во).

        // Печатаем на консоль драйвера план выполнения RDD
        System.out.println(counts.toDebugString());

        // В этот момент код еще не выполнился,
        // реальное выполнение произойдет когда выполнится терминальная операция,
        // например, сохранение результата в место (указано вторым аргументом командной строки)

        counts.saveAsTextFile(args[1]);
        //System.out.println(counts.take(10));

        List<String> top10Words = counts
                .mapToPair(Tuple2::swap)
                .sortByKey(false)
                .repartition(1)
                .map(pair -> pair._2 + " " + pair._1)
                .take(10);

        System.out.println(top10Words);

        List<String> bottom10Words = counts
                .mapToPair(Tuple2::swap)
                .sortByKey(true)
                .repartition(1)
                .map(pair -> pair._1() + " " + pair._2())
                .take(10);

        System.out.println(bottom10Words);
    }
}
