package ru.mai.dep806.bigdata.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;

/**
 * Пример Word Count на Spark / Java 8.
 * <p>
 * Запуск на Hadoop:
 * <p>
 * spark-submit --class ru.mai.dep806.bigdata.spark.WordCount \
 * --master local \
 * --deploy-mode cluster \
 * --driver-memory 100M \
 * --executor-memory 500M \
 * spark-jobs-1.0-SNAPSHOT.jar \
 * /user/stud/eugene/library \
 * /user/stud/eugene/spark-wc
 * <p>
 * <p>
 * DAG:
 * <p>
 * (3) ShuffledRDD[4] at reduceByKey at WordCount.java:57 [Disk Memory Deserialized 1x Replicated]
 * +-(3) MapPartitionsRDD[3] at mapToPair at WordCount.java:55 [Disk Memory Deserialized 1x Replicated]
 * |  MapPartitionsRDD[2] at flatMap at WordCount.java:49 [Disk Memory Deserialized 1x Replicated]
 * |  /user/stud/texts MapPartitionsRDD[1] at textFile at WordCount.java:47 [Disk Memory Deserialized 1x Replicated]
 * |  /user/stud/texts HadoopRDD[0] at textFile at WordCount.java:47 [Disk Memory Deserialized 1x Replicated
 * <p>
 * <p>
 * spark-submit --class ru.mai.dep806.bigdata.spark.WordCount hdfs:///user/stud/eugene/spark-jobs-1.0-SNAPSHOT.jar  /user/stud/texts /user/stud/words3
 */
public class RefactoredWordCount {

    public static void main(String[] args) {

        // Создание спарк-конфигурации и контекста
        SparkConf sparkConf = new SparkConf().setAppName("Spark Word Count");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);

        // Чтение файлов из директории, указанной первым аргументом командной строки
        JavaRDD<String> words = sc.textFile(args[0]);
        transform(words)
                .saveAsTextFile(args[1]);
    }

    public static JavaPairRDD<String, Integer> transform(JavaRDD<String> input) {
        return input.flatMap((String line) -> Arrays.asList(line.split(" ")).iterator())
                .filter(word -> word.trim().length() > 0)
                .mapToPair(word -> new Tuple2<>(word, 1))
                // Выполняем группировку по ключу (слову), суммируя значения
                .reduceByKey((x, y) -> x + y);

    }
}
