package ru.mai.dep806.bigdata.spark;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class WordCountTest {

    @Test
    public void testTransform() {
        // Prepare
        SparkConf sparkConf = new SparkConf().setAppName("Test Transform");
        sparkConf.setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(sparkConf);
        JavaRDD<String> rdd = sc.parallelize(Arrays.asList(
                "word1 word2 word3",
                "word2 word3 word4",
                "", // empty line
                "word1"
        ));

        // Act
        JavaPairRDD<String, Integer> results = RefactoredWordCount.transform(rdd);

        // assert
        Map<String, Integer> resultMap = results.collectAsMap();
        Map<String, Integer> expected = new HashMap<>();
        expected.put("word1", 2);
        expected.put("word2", 2);
        expected.put("word3", 2);
        expected.put("word4", 1);
        Assert.assertEquals(expected, resultMap);
    }

}
