package ru.mai.dep806.bigdata.mr;

import org.apache.hadoop.util.bloom.BloomFilter;
import org.apache.hadoop.util.bloom.Key;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BloomFilterTest {
    @Test
    public void testBloomFilter() {
        BloomFilter filter = HighReputationUsersBloomFilter.createBloomFilter();
        String[] filterSet = new String[] {"1", "5", "7"};
        String[] testSet = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
        for (String item : filterSet) {
            filter.add(new Key(item.getBytes()));
        }
        for (String item: testSet) {
            System.out.println(item + " is " + filter.membershipTest(new Key(item.getBytes())));
        }
    }

    @Test
    public void testDistributedBloomFilter() {
        String[][] filterSets = new String[][] {
                new String[] {"1", "5", "7"},
                new String[] {"3", "8"},
                new String[] {"10"}
        };
        String[] testSet = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};

        List<BloomFilter> filters = new ArrayList<>();
        for (String[] filterSet: filterSets) {
            BloomFilter filter = HighReputationUsersBloomFilter.createBloomFilter();
            for (String item : filterSet) {
                filter.add(new Key(item.getBytes()));
            }
            filters.add(filter);
        }
        BloomFilter aggregatedFilter = HighReputationUsersBloomFilter.createBloomFilter();
        for (BloomFilter subFilter: filters) {
            aggregatedFilter.or(subFilter);
        }

        for (String item: testSet) {
            System.out.println(item + " is " + aggregatedFilter.membershipTest(new Key(item.getBytes())));
        }
    }
}
